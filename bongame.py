import pygame
import random
import time
import os
import random

pygame.init()

clock = pygame.time.Clock()
screen = pygame.display.set_mode((1920, 1080))
bg = pygame.image.load("/home/bonbon/game/back.png")
x11 = - 340
y11 = 800
x12 = - 200
y12 = 800
x13 = -60
y13 = 800
x21 = 1320
y21 = 180
x22 = 1480
y22 = 180
x23 = 1620
y23 = 180
logo = pygame.image.load("/home/bonbon/game/logo.png")
screen.blit(logo, (0, 0))
player1 = pygame.image.load("/home/bonbon/game/player1.png")
player10 = pygame.image.load("/home/bonbon/game/player1d.png")
player11 = random.choice([player1, player10])
screen.blit(player11, (x11, y11))
player2 = pygame.image.load("/home/bonbon/game/player1.png")
player11 = pygame.image.load("/home/bonbon/game/player1d.png")
player12 = random.choice([player2, player11])
screen.blit(player12, (x12, y12))
player3 = pygame.image.load("/home/bonbon/game/player1.png")
player13 = pygame.image.load("/home/bonbon/game/player1d.png")
player15 = random.choice([player3, player13])
screen.blit(player15, (x13, y13))
player4 = pygame.image.load("/home/bonbon/game/player2.png")
player7 = pygame.image.load("/home/bonbon/game/player2d.png")
player21 = random.choice([player4, player7])
screen.blit(player21, (x21, y21))
player5 = pygame.image.load("/home/bonbon/game/player2.png")
player8 = pygame.image.load("/home/bonbon/game/player2d.png")
player22 = random.choice([player5, player8])
screen.blit(player22, (x22, y22))
player6 = pygame.image.load("/home/bonbon/game/player2.png")
player9 = pygame.image.load("/home/bonbon/game/player2d.png")
player23 = random.choice([player6, player9])
screen.blit(player23, (x23, y23))
pygame.display.set_caption("Bonbon`s Tower")
active = True
while active:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            active = False
    player21 = random.choice([player4, player7])
    player22 = random.choice([player5, player8])
    player23 = random.choice([player6, player9])
    player11 = random.choice([player1, player10])
    player12 = random.choice([player2, player11])
    player15 = random.choice([player3, player13])
    if x13 <= 1620 :
        x11 = x11 + 20
        x12 = x12 + 20
        x13 = x13 + 20
    if x13 >= 1620 :
        x11 = 1320
        x12 = 1480
        x13 = 1620
    if x13 >= 1620 and y13 >= 180:
        y13 = y13 - 20
        y12 = y12 - 20
        y11 = y11 - 20
    if y23 >= -200  and x23 >= 1620:
        y23 = y23 - 5
        y22 = y22 -5
        y21 = y21 - 5
    if y23 <= -200 :
        y23 = 800
        y22 = 800
        y21 = 800
        x23 =  - 60
        x22 = - 200
        x21 = - 340
    if x23 <= 1620 :
        x21 = x21 + 20
        x22 = x22 + 20
        x23 = x23 + 20
    if x23 >= 1620 :
        x21 = 1320
        x22 = 1480
        x23 = 1620
    if x23 >= 1620 and y23 >= 180:
        y23 = y23 - 20
        y22 = y22 - 20
        y21 = y21 - 20
    if y13 >= -200  and x13 >= 1620:
        y13 = y13 - 5
        y12 = y12 -5
        y11 = y11 - 5
    if y13 <= -200 :
        y13 = 800
        y12 = 800
        y11 = 800
        x13 = - 60
        x12 = - 200
        x11 = - 340
    screen.blit(bg, (0,0))
    screen.blit(player11, (x11, y11))
    screen.blit(player12, (x12, y12))
    screen.blit(player15, (x13, y13))
    screen.blit(player21, (x21, y21))
    screen.blit(player22, (x22, y22))
    screen.blit(player23, (x23, y23))
    screen.blit(logo, (0, 0))
    pygame.display.update()
    clock.tick(60)